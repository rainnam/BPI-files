#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-beta3' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-r2.conf -I '-sd-emmc' -V '-pretest-wayland' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-r2.conf -I '-sd-emmc' -V '-preview' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-beta' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-beta' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-beta2' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2m.conf -I '-sd-emmc' -V '-preview' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2m.conf -I '-sd-emmc' -V '-preview2' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2m.conf -I '-sd-emmc' -V '-preview' -c ubuntu-mate-ros-tensorflow-from-sd.conf
#bpi-migrate -c bpi-m2m.conf -I '-sd-16GB' -V '-preview2' -c ubuntu-mate-ros-tensorflow-from-sd.conf
#bpi-migrate -c bpi-m2m.conf -I '-sd-emmc' -V '-preview3' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-preview' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-preview2' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m1-m1p-r1.conf -I '-sd-emmc' -V '-preview2' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m64.conf -I '-sd-emmc' -V '-preview2' -c ubuntu-mate-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-preview' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m64.conf -I '-sd-emmc' -V '-preview' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m3.conf -I '-sd-emmc' -V '-preview' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m3.conf -I '-sd-emmc' -V '-preview2' -c ubuntu-server-from-sd.conf
#
## 20180115
#
#bpi-migrate -c bpi-r1-linux4.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2z.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m3.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2m.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m64.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m1-m1p-r1.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#
## 20180227
#
#bpi-migrate -c bpi-m2p-linux4.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2p-linux4.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-v1.0' -c ubuntu-server-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-demo' -c kano-from-sd.conf
#bpi-migrate -c bpi-m2z.conf -I '-sd-emmc' -V '-demo' -c kano-from-sd.conf
#
## 20180305
#
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-demo' -c kano-org-from-sd.conf
#bpi-migrate -c bpi-m2u.conf -I '-sd-emmc' -V '-20180227-demo' -c kano-from-sd.conf
#bpi-migrate -c bpi-m2z.conf -I '-sd-emmc' -V '-20180227-demo' -c kano-from-sd.conf
#bpi-migrate -c bpi-m2p-linux4.conf -I '-sd-emmc' -V '-20180227-demo' -c kano-from-sd.conf
#bpi-migrate -c bpi-m64.conf -I '-sd-emmc' -V '-20180227-demo' -c kano-from-sd.conf
#bpi-migrate -c bpi-m64-linux4.conf -I '-sd-emmc' -V '-20180227-demo' -c kano-from-sd.conf
#bpi-migrate -c bpi-m64.conf -I '-sd-emmc' -V '-demo' -c raspbian-stretch-from-sd.conf
#
## 20181120
#
#bpi-migrate -c bpi-m2p-linux4.conf -I '-sd-emmc' -V '-20181120-demo' -c kano-from-sd.conf -b /media/mikey/kanux-4.1.0 -r /media/mikey/rootfs
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-demo' -c kano-from-sd.conf 
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-nogpu-demo' -c kano-from-sd.conf 
#
## 20181121
#
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-mali-gpu-demo' -c kano-from-sd.conf 
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-mali-gpu-demo2' -c kano-from-sd.conf 
#bpi-migrate -c bpi-m2p-linux4.conf -I '-sd-emmc' -V '-demo' -c debian-xfce-from-sd.conf -r /media/mikey/891ab791-40ff-4194-b4af-9f888d9358fb
#
## 20181122
#
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-demo' -c debian-xfce-from-sd.conf 
#
## 20181204
#
#bpi-migrate -c bpi-w2.conf -I '-sd-emmc' -V '-demo' -c debian-xfce-from-sd.conf 
#
## 20181212
#
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-debian9-demo' -c kano-from-sd.conf 
#
## 20181212
#
#bpi-migrate -c bpi-m2p.conf -I '-sd-emmc' -V '-debian9-demo2' -c kano-from-sd.conf 
#
## 20190117
#
#bpi-migrate -c bpi-w2.conf -I '-sd-emmc' -V '-debian9-demo3' -c kano-from-sd.conf 
#
## 20191108
#
bpi-migrate -c bpi-m64-linux4.4-1080p.conf -I '-sd-emmc' -V '-demo' -c ubuntu-mate-from-sd.conf BPI_BOOT_UUID=533D3A93 BPI_ROOT_UUID=63cb561a-1cae-42f1-bb18-394ff8aa6365
